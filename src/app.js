import React from 'react';
import Form from './Form';
import NotesList from './NotesList';

import css from './style.css';
import img from './img/jpg.jpg';

class App extends React.Component {
	constructor() {
		super();

		this.state = {
			notes: [],
		};
	}

	componentWillMount() {
		const localNotes = JSON.parse(localStorage.getItem('notes'));
		if (localNotes) {
			this.setState({
				notes: localNotes,
			});
		}
	}

	componentDidUpdate() {
		localStorage.setItem('notes', JSON.stringify(this.state.notes));
	}

	addNewNote(note) {
		this.setState({
			notes: [note, ...this.state.notes],
		});
	}

	removeNote(note) {
		this.setState({
			notes: this.state.notes.filter(e => e.id !== note),
		});
	}

	changeNote(note) {
		const index = this.state.notes.findIndex(e => e.id === note.id);
		this.state.notes[index].text = note.text;
		this.setState({
			notes: this.state.notes,
		});
	}

	render() {
		const AppVersion = '[AIV]{version}[/AIV]';
		console.log(`Current app version: ${AppVersion}`);
		return (
			<div className="app">
				<h6>app version: {AppVersion}</h6>
				<img src={img} />
				<h1 className="app__header">Notes</h1>

				<Form handleNote={this.addNewNote.bind(this)} />

				<NotesList
					notes={this.state.notes}
					removeNote={this.removeNote.bind(this)}
					changeNote={this.changeNote.bind(this)}
				/>
				{this.props.title}
			</div>
		);
	}
}

class InputWrapper extends React.Component {
	render() {
		return (
			<form>
				<label htmlFor="inp">From hoc: </label>
				<input type="text" name="inp" {...this.props} />
			</form>
		);
	}
}

// Hight order component!
const hoc = Component => {
	return class Inner extends React.Component {
		constructor(props) {
			super(props);

			this.state = {
				name: '',
			};

			this.handleChange = this.handleChange.bind(this);
		}

		handleChange(e) {
			console.log('E', e.target.value);
			this.setState({
				name: e.target.value,
			});
		}

		render() {
			const newProps = {
				name: {
					value: this.state.name,
					onChange: this.handleChange,
				},
			};

			return (
				<div>
					<Component {...this.props} />
					<br />
					<InputWrapper {...newProps.name} />
				</div>
			);
		}
	};
};

export default hoc(App);

//export default App;
