import React from 'react';
import PropTypes from 'prop-types';

class Form extends React.Component {
	constructor() {
		super();

		this.state = {
			text: '',
		};

		this.changeText = this.changeText.bind(this);
		this.submitForm = this.submitForm.bind(this);
	}

	changeText(e) {
		this.setState({
			text: e.target.value,
		});
	}

	clearText() {
		this.setState({
			text: '',
		});
	}

	submitForm(e) {
		e.preventDefault();
		if (this.state.text) {
			const newNote = {
				id: Date.now(),
				text: this.state.text,
			};
			this.props.handleNote(newNote);
			this.clearText();
		}
	}

	render() {
		return (
			<form onSubmit={this.submitForm}>
				<textarea placeholder="write note" name="text" value={this.state.text} onChange={this.changeText} />
				<button type="submit">submit</button>
			</form>
		);
	}
}

Form.propTypes = {
	handleNote: PropTypes.func.isRequired,
};

export default Form;
