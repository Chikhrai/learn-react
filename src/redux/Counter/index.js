import React from 'react';
import Counter from './component';
import { Provider } from 'react-redux';
import store from './store';

const Comp = () => {
    return (
        <Provider store={store}>
            <Counter />
        </Provider>
    )
}

export default Comp;