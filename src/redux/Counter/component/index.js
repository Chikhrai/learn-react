import React from 'react';
// import store from '../store';
import { connect } from 'react-redux';
import { increment, decrement } from '../actions';

const Counter = (props) => {
    return( 
        <div>
            <button
                onClick={() => props.decrement(10)}
            >-10</button>
            <button
                onClick={() => props.decrement(1)}
            >-1</button>
            {props.count}
            <button
                onClick={() => props.increment(1)}
            >1</button>
            <button
                onClick={() => props.increment(10)}
            >10</button>
        </div>
    )
}

const mapStateToProps = (state) => {
    console.log(state);
    return {count: state};
};

// const mapDispatchToProps = (dispatcher) => {
//     return {
//         'increment': () => dispatcher({type: "Inc"}),
//         'decrement': () => dispatcher({type: "Dec"}),
//     };
// }

export default connect(mapStateToProps, { increment, decrement })(Counter);