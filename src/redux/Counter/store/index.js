import { createStore } from 'redux';

const counter = (count = 0, action) => {
    switch(action.type) {
        case "Inc": {
            return count + action.value;
        }
        case "Dec": {
            return count - action.value;
        }
        default: {
            return count;
        }
    }
}

export default createStore(counter);

