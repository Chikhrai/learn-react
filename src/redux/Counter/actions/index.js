export const increment = (value) => ({type: "Inc", value});
export const decrement = (value) => ({type: "Dec", value});

