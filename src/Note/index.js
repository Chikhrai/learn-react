import React from 'react';
import PropTypes from 'prop-types';
class Note extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isChange: false,
			text: this.props.children,
		};

		this.changeItem = this.changeItem.bind(this);
		this.removeItem = this.removeItem.bind(this);
		this.handleInput = this.handleInput.bind(this);
		this.updateItem = this.updateItem.bind(this);
	}

	removeItem(id) {
		this.props.removeNote(id);
	}

	handleInput(e) {
		this.setState({
			text: e.target.value,
		});
	}

	changeItem() {
		this.setState({
			isChange: true,
		});
	}

	updateItem(id) {
		// can change on this.props.id
		return e => {
			e.preventDefault();
			this.props.changeNote({ id, text: this.state.text });
			this.setState({
				isChange: false,
			});
		};
	}

	render() {
		const { children, id } = this.props;
		return (
			<div className="app__note">
				<button onClick={this.changeItem}>i</button>
				{!this.state.isChange ? (
					children
				) : (
					<form onSubmit={this.updateItem(id)}>
						<input value={this.state.text} onChange={this.handleInput} />
						<button type="submit">u</button>
					</form>
				)}
				<button onClick={() => this.removeItem(id)}>x</button>
			</div>
		);
	}
}

Note.propTypes = {
	id: PropTypes.number.isRequired,
	children: PropTypes.string.isRequired,
	removeNote: PropTypes.func.isRequired,
	changeNote: PropTypes.func.isRequired,
};

export default Note;
