import EventEmitter from 'events';

const notes = [];

class AppStore extends EventEmitter {
    constructor() {
        super();
    }

    emitChange() {
        console.log(notes);
        this.emit("ch");
    }

    addListener(cb) {
        this.on('ch', cb);
    }

    removeListener(cb) {
        this.removeListener('ch', cb);
    }

    getNotes() {
        return notes;
    }

	addNewNote(note) {
        notes.unshift(note);
        this.emitChange();
    }

    changeNote(noteId, newText) {
        const noteIndex = notes.findIndex(e => e.id === noteId);
        console.log(notes, noteIndex);
        if(noteIndex >= 0) {
            notes[noteIndex] = Object.assign(notes[noteIndex], {text: newText});
        }
        this.emitChange();
    } 
    
	removeNote(note) {
        const noteIndex = notes.findIndex(e => e.id === note);
        if(noteIndex >= 0) {
            notes.splice(noteIndex, 1);
        }
        this.emitChange();
	}

}

export default new AppStore();