import { Dispatcher } from 'flux';
import AppStore from '../store';

const AppDispatcher = new Dispatcher();

AppDispatcher.register(action => {
	console.log(action);
	if (action.type === 'Add') {
        AppStore.addNewNote(action.note);
	} else if (action.type === 'Remove') {
		AppStore.removeNote(action.note);
	} else if (action.type === 'Update') {
        AppStore.changeNote(action.note, action.text);
    }
});

export default AppDispatcher;
