import React from 'react';
import Form from './components/Form';
import NotesList from './components/NotesList';
import AppStore from './store';

class App extends React.Component {
	constructor() {
		super();

		this.state = {
			notes: AppStore.getNotes(),
		};
	}

	render() {
		const AppVersion = '[AIV]{version}[/AIV]';
		console.log(`Current app version: ${AppVersion}`);
		return (
			<div className="app">
				<h6>app version: {AppVersion}</h6>
				<h1 className="app__header">Notes</h1>

				<Form />

				<NotesList />
			</div>
		);
	}
}

export default App;
