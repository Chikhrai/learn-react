import React from 'react';
import Note from '../Note';
import AppStore from '../../store';

class NotesList extends React.Component {
	constructor() {
		super();

		this.state = {
			notes: AppStore.getNotes()
		};
		this.updateState = this.updateState.bind(this);
	}

	componentDidMount() {
        AppStore.addListener( this.updateState );
    }

    componentWillUnmount() {
        AppStore.removeListener( this.updateState );
	}
	
	updateState() {
		this.setState({
			notes: AppStore.getNotes(),
		});
	}

	render() {
		const { notes } = this.state;
		console.log(notes);
		return (
			<div className="app__noteslist">
				{notes.map(note => {
					return (
						<Note key={note.id} id={note.id}>
							{note.text}
						</Note>
					);
				})}
			</div>
		);
	}
}

export default NotesList;
