import React from 'react';
import EventEmitter from 'events';
import { Dispatcher } from 'flux';

let count = 0;

const Store = Object.assign({}, EventEmitter.prototype, {
    emitChange() {
        this.emit("CHANGE");
    },
    addListener(cb) {
        this.on("CHANGE", cb);
    },
    removeListener(cb) {
        this.removeListener("CHANGE", cb);
    },
    getCount() {
        return count;
    }
});

const AppDispatcher = new Dispatcher();
AppDispatcher.register(action => {
    console.log(action);
    if(action.type==="INCREMENT"){
        count++;
        Store.emitChange();
    } else if (action.type==="DECREMENT") {
        count--;
        Store.emitChange();
    }
});

class Counter extends React.Component {
    constructor() {
        super();

        this.state = this.getState();

        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        Store.addListener( this.updateState );
    }

    componentWillUnmount() {
        Store.removeListener( this.updateState );
    }

    updateState() {
        this.setState({
            count: Store.getCount()
        });
    }

    getState() {
        return {
            count: Store.getCount()
        }
    }

    render() {
        return( 
            <div>
                <button
                    onClick={() => AppDispatcher.dispatch({type: "DECREMENT"})}
                >-1</button>
                {this.state.count}
                <button
                    onClick={() => AppDispatcher.dispatch({type: "INCREMENT"})}
                >1</button>
            </div>
        )
    }
}

export default Counter;