import React from 'react';
import PropTypes from 'prop-types';
import Note from '../Note';

class NotesList extends React.Component {
	render() {
		const { notes } = this.props;
		return (
			<div className="app__noteslist">
				{notes.map(note => {
					return (
						<Note key={note.id} id={note.id} removeNote={this.props.removeNote} changeNote={this.props.changeNote}>
							{note.text}
						</Note>
					);
				})}
			</div>
		);
	}
}

NotesList.propTypes = {
	notes: PropTypes.array.isRequired,
	removeNote: PropTypes.func.isRequired,
	changeNote: PropTypes.func.isRequired,
};

export default NotesList;
