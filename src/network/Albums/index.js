import React from 'react';

class Albums extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const albumsData = this.props.data.album;
		console.log('D', albumsData);
		return (
			<div>
				{albumsData.map(album => (
					<div key={album.idAlbum}>
						<img style={{ width: 200 }} src={album.strAlbumThumb} alt={album.strAlbum} />
						<h3>{album.strAlbum}</h3>
					</div>
				))}
			</div>
		);
	}
}

export default Albums;
