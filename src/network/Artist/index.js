import React from 'react';
import Albums from '../Albums';
import PropTypes from 'prop-types';
import Loader from '../Loader';

const Hoc = Loader(Albums);

class Artist extends React.Component {
	constructor() {
		super();
	}

	render() {
        const {handleError} = this.props;
        const data = this.props.data.artists;
		return (
            <div>
                { 
                    data.map( (element) => (
                        <div key={element.idArtist}>
                            <img
                                src={element.strArtistBanner || `http://placehold.it/1000x185/dddddd/000000?text=${element.strArtist}`}
                                alt={element.strArtist}
                            />
                            <h2>{element.strArtist}</h2>
                            <p>{element.strBiographyEN}</p>

                            <Hoc id={element.idArtist} handleError={handleError} />
                        </div>
                    ))
                }
            </div>
        );
	}
}

Artist.propTypes = {
    data: PropTypes.object.isRequired,
};

export default Artist;
