import React from 'react';
import Artist from './Artist';
import Loader from './Loader';

const Hoc = Loader(Artist);

class App extends React.Component {
	constructor() {
		super();

		this.state = {
			isLoading: false,
			search: '',
		};

		this.handleChange = this.handleChange.bind(this);
		this.submitForm = this.submitForm.bind(this);
		this.handleError = this.handleError.bind(this);
	}

	handleChange(e) {
		this.setState({
			search: e.target.value,
		});
	}

	handleError(e) {
		console.log('ERROR!', e);
	}

	submitForm(e) {
		e.preventDefault();
		this.setState({
			isLoading: true,
		});
	}

	render() {
		const { search, isLoading } = this.state;
		return (
			<div>
				<form onSubmit={this.submitForm}>
					<input type="text" onChange={this.handleChange} value={search} />
					<button type="submit">Find</button>
				</form>
				{isLoading ? <Hoc search={search} handeError={this.handleError} /> : ''}
			</div>
		);
	}
}

export default App;
