import React from 'react';
import loaderImg from '../../img/loader.gif';
import axios from 'axios';

const Loader = (Component, props) => {
	return class extends React.Component {
		constructor(props) {
			super(props);

			this.state = {
				isLoading: true,
				data: null,
				error: null,
			};

			this.loadData();
		}

		disableLoader() {
			this.setState({
				isLoading: false,
			});
		}

		loadData() {
			let url;
			if (this.props.search) {
				url = `https://theaudiodb.com/api/v1/json/1/search.php?s=${encodeURIComponent(this.props.search)}`;
			} else {
				url = `https://theaudiodb.com/api/v1/json/1/album.php?i=${this.props.id}`;
			}

			console.log('Loading...', this.props.search);
			axios(url)
				.then(data => {
					console.log('DATA', data);
					this.setState(
						{
							data: data.data,
						},
						this.disableLoader,
					);
				})
				.catch(e => {
					console.error('Fetch err:', e);
					this.setState(
						{
							error: e,
						},
						this.disableLoader,
					);
				});
		}

		render() {
			const { isLoading, data, error } = this.state;
			if (isLoading) {
				return <img src={loaderImg} alt="loading..." />;
			} else if (error) {
				return this.props.handleError(error);
			} else {
				return <Component data={data} handleError={this.props.handleError} />;
			}
		}
	};
};

export default Loader;
