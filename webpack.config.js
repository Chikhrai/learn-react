/** @format */

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const WebpackAutoInject = require('webpack-auto-inject-version');
const webpack = require('webpack');

module.exports = {
	entry: './src/main.js',
	output: {
		path: path.resolve(__dirname, 'prod'),
		filename: 'build.js',
	},
	mode: process.env.NODE_ENV,
	devtool: process.env.NODE_ENV === 'development' && 'cheap-source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: [{ loader: 'babel-loader' }, { loader: 'eslint-loader' }],
			},
			{
				test: /\.css/,
				exclude: /node_modules/,
				// use:['style-loader', 'css-loader'],
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: {
						loader: 'css-loader',
						options: {
							modules: true,
							localIdentName: process.env.NODE_ENV === 'development' ? '[name]__[local]' : '[hash:base64:8]',
						},
					},
				}),
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				exclude: /node_modules/,
				use: {
					loader: 'file-loader',
					options: {
						name: 'img/[name].[ext]',
					},
				},
			},
		],
	},
	plugins: [
		new ExtractTextPlugin('styles.css'),
		new CopyWebpackPlugin([
			{
				from: path.resolve(__dirname, 'src', 'index.html'),
				to: path.resolve(__dirname, 'prod'),
			},
		]),
		new WebpackAutoInject({
			components: {
				AutoIncreaseVersion: true,
			},
			componentsOptions: {
				AutoIncreaseVersion: {
					runInWatchMode: false, // it will increase version with every single build!
				},
			},
		}),
		new webpack.DefinePlugin({
			// __DEV__: `'${process.env.npm_package_version}'`, // previous version =(
			// __DEV__: JSON.stringify(require('./package.json').version)
		}),
	],
};
