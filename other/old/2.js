const Card = React.createClass({
  render() {
    const { name, avatarUrl } = this.props;

    return(<div className="card">
      <h2 className="card__author-name">{name}</h2>
      <img src={avatarUrl} alt/>
    </div>)
  }
});

const CardList = React.createClass({
  render() {
    const { title, children } = this.props;
    return (
      <div>
        <h1>{title}</h1>

        <div className="card-list">
        <Card
          name="Valera"
          avatarUrl="http://apikabu.ru/img_n/2012-02_6/5tm.jpg"
        />

        <Card
          name="Valera"
          avatarUrl="https://i.ytimg.com/vi/fPsb45eUQJY/maxresdefault.jpg"
        />
        </div>

        <p>{children}</p>
      </div>
    )
  }
});

ReactDOM.render(
  <CardList title="Our best students" >Hello</CardList>,
  document.getElementById('app')
);
