// const Counter = React.createClass({
//   getInitialState () {
//     return{
//       count: 0
//     }
//   },

//   handleClick(num) {
//     return () => {
//       console.log("before", this.state.count);
//       this.setState({
//         count: this.state.count + num
//       }, () => {
//         console.log("after", this.state.count);
//       });
//     }
//   },

//   render() {
//     const {count} = this.state;
//     return(
//       <div className="counter">
//         <button onClick={this.handleClick(1)}>+1</button>
//         {count}
//         <button onClick={this.handleClick(-1)}>-1</button>
//       </div>
//     )
//   }
// });

// ReactDOM.render(
//   <Counter/>,
//   document.getElementById('app')
// );

// 1st render:
// - getDefaultProps => при инициализации
// - getInitialState
// - componentWillMount
// - render
// - componentDidMount

// props change
// - componentWillReceiveProps
// - shouldComponentUpdate <= true/false
// - componentWillUpdate
// - render
// - componentDidUpdate

// state change
// - shouldComponentUpdate <= true/false
// - componentWillUpdate
// - render
// - componentDidUpdate

// unmount
// - componentWillUnmount

// const App = React.createClass({
//   getInitialState() {
//     return {
//       isShow: false,
//       count: 0
//     };
//   },

//   handleClick() {
//     this.setState({
//       count: ++this.state.count
//     });
//   },

//   handleShowClick() {
//     this.setState({
//       isShow: !this.state.isShow
//     });
//   },

//   render() {
//     return (
//       <div>
//         <button onClick={this.handleClick}>{this.state.count}</button>
//         <button onClick={() => this.handleShowClick()}>
//           {this.state.isShow ? "Hide" : "Show"}
//         </button>
//         {this.state.isShow ? <Test id={this.state.count} /> : <p>empty</p>}
//       </div>
//     );
//   }
// });

// const Test = React.createClass({
//   getDefaultProps() {
//     console.log("getDefaultProps");
//     return {
//       count: 0
//     };
//   },

//   getInitialState() {
//     console.log("getInitialState");
//     return {
//       count: 0
//     };
//   },

//   componentWillMount() {
//     console.log("componentWillMount");
//   },

//   componentDidMount() {
//     console.log("componentDidMount");
//   },

//   componentWillReceiveProps() {
//     console.log("componentWillReceiveProps");
//   },

//   componentWillUpdate(nextProps, nextState) {
//     console.log("componentWillUpdate");
//   },

//   componentDidUpdate(prevProps, prevState) {
//     console.log("componentDidUpdate");
//   },

//   shouldComponentUpdate(nextProps, nextState) {
//     console.log(this.props, nextProps);
//     console.log(this.state, nextState);
//     console.log( "shouldComponentUpdate"    );

//     return this.props.id !== nextProps.id;
//   },

//   componentWillUnmount() {
//     console.log("componentWillUnmount");
//   },

//   updateCount() {
//     this.setState({
//       count: ++this.state.count
//     });
//   },

//   render() {
//     console.log("render");
//     return (
//       <div>
//         <p>{this.props.id}</p>
//         <button onClick={this.updateCount}>Update count</button>
//       </div>
//     );
//   }
// });

const Timer = React.createClass({
  getInitialState() {
    return {
      time: 0
    }
  },

  tick() {
    console.log("tick");
    this.setState({
      time: ++this.state.time
    });
  },

  componentDidMount() {
    this.timer = setInterval(this.tick, 1000); 
  },

  componentWillUnmount() {
    clearInterval(this.timer);
  },

  render() {
    return (
        <div>{this.state.time}</div>
    )
  }
});

const App = React.createClass({
  getInitialState() {
    return {
      isMount: false
    };
  },

  showElement() {
    this.setState({
      isMount: !this.state.isMount
    })
  },

  render() {
    return (
      <div>
        <button onClick={this.showElement}>{this.state.isMount ? "Hide": "Show" }</button>
        {this.state.isMount ? <Timer /> : null}
      </div>
    )
  }
});

ReactDOM.render(<App />, document.getElementById("app"));


