const Hello = React.createClass({

  getInitialState () {
    return {
      isShow: false,
      count: 0,
      text:''
    }
  },

   handleClick  ()  {
     this.setState(prevState => ({
       isShow: !prevState.isShow,
       count: ++this.state.count
     }));
  },

  handleText  (e)  {
    this.setState({
      text: e.target.value
    });

    console.log(e.target.value)
  },

  render(){
    const { isShow, count, text } = this.state;
    return(<div>
      <input type="text" placeholder="Enter text" value={text} onChange={this.handleText}/>

      <p>{text || 'default text'}</p>

      {/*<button onClick={this.handleClick}>{count}</button>*/}

      {/*{isShow ?(*/}
        {/*<p>Loremipsum</p>*/}
      {/*): null}*/}

    </div>)
  }
});

ReactDOM.render(
  <Hello/>,
  document.getElementById('app')
);
