const Counter = React.createClass({
  getInitialState () {
    return{
      count: 0
    }
  },

  handleIncrement () {
    this.setState( {
      count: ++this.state.count
    })
  },

  handleDecrement () {
    this.setState( {
      count: --this.state.count
    })
  },

  handleCount (type) {
    this.setState({
      count: type ==='plus' ?  ++this.state.count : --this.state.count
    })
  },

  render() {
    const {count} = this.state;
    return(
      <div className="counter">
        <button onClick={()=> this.handleCount('plus')}>+1</button>
        {count}
        <button onClick={()=> this.handleDecrement('minus')}>-1</button>
      </div>
    )
  }
});


ReactDOM.render(
  <Counter/>,
  document.getElementById('app')
);
