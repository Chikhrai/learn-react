
const Form = React.createClass({

    getInitialState() {
        return {
            text: ''
        }
    },

    changeText(e) {
        this.setState({
            text: e.target.value
        })
    },

    clearText() {
        this.setState({
            text: ''
        });
    },

    submitForm(e){
        e.preventDefault();
        if(this.state.text) {
            const newNote = {
                id: Date.now(),
                text: this.state.text
            };
            this.props.handleNote(newNote);
            this.clearText();
        }
    },

    render() {
        return (
            <form onSubmit={this.submitForm}>
                <textarea
                    placeholder="write note"
                    name="text"
                    value={this.state.text}
                    onChange={this.changeText}
                />
                <button type="submit">submit
                </button>
            </form>
        );
    }
});

const Note = React.createClass({
    getInitialState() {
        return {
            isChange: false,
            text: this.props.children
        }
    },

    removeItem(id) {
        this.props.removeNote(id);
    },

    handleInput(e) {
        this.setState({
            text: e.target.value
        });
    },

    changeItem() {
        this.setState({
            isChange: true
        });
    },

    updateItem(id) {
        // can change on this.props.id
        return (e) => {
            e.preventDefault();
            this.props.changeNote({id, text: this.state.text});
            this.setState({
                isChange: false
            });
        }
    },

    render() {
        const { children, id } = this.props;
        return (
            <div className="app__note">
                <button onClick={this.changeItem}>i</button>
                { !this.state.isChange ? 
                    children : 
                    <form onSubmit={this.updateItem(id)}>
                        <input value={this.state.text} onChange={this.handleInput}/>
                        <button type="submit">u</button>
                    </form>
                }
                <button onClick={() => this.removeItem(id)}>x</button>
            </div>
        )
    }
});

const NotesList = React.createClass({
    render() {
        const { notes } = this.props;
        return (
            <div className="app__noteslist">
            {
                notes.map( note => {
                    return (
                        <Note key={note.id} id={note.id} removeNote={this.props.removeNote} changeNote={this.props.changeNote}>
                            {note.text}
                        </Note>
                    )
                })
            }
            </div>
        )
    }
});

const App = React.createClass({
    getInitialState() {
        return {
            notes: []
        }
    },

    componentWillMount() {
        const localNotes = JSON.parse(localStorage.getItem("notes"));
        if(localNotes){
            this.setState({
                notes: localNotes
            });
        }
    },

    componentDidUpdate() {
        localStorage.setItem("notes", JSON.stringify(this.state.notes));
    },

    addNewNote(note) {
        this.setState({
            notes: [note, ...this.state.notes]
        });
    },

    removeNote(note) {
        this.setState({
            notes: this.state.notes.filter(e => e.id !== note)
        });
    },

    changeNote(note) {
        const index = this.state.notes.findIndex(e => e.id === note.id);
        this.state.notes[index].text = note.text;
        this.setState({
            notes: this.state.notes
        });
    },

    render() {
        return (
            <div className="app">
                <h1 className="app__header">Notes</h1>

                <Form handleNote={this.addNewNote}/>

                <NotesList notes={this.state.notes} removeNote={this.removeNote} changeNote={this.changeNote}/>
            </div>
        )
    }
});


ReactDOM.render(
    <App/>,
    document.getElementById('app')
);