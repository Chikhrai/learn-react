// Получить из массива пользователей
const users = [
	{
		name: "Ivan",
		surname: "Ivanov",
		interests: ["computers", "food"],
	},
	{
		name: "Petya",
		surname: "Petrov",
		interests: ["computers", "food", "cars"],
	},
	{
		name: "Sidr",
		surname: "Sidorov",
		interests: ["cars", "math"],
	},
	{
		name: "Valera",
		surname: "Ololo",
		interests: ["computers", "food", "math"],
	},
];
// объект с распределением по интересам:
// {
//     computers: 3,
//     food: 3,
//     math: 2,
//     cars: 2
// }
const result = users.reduce((acc, current) => {
	current.interests.forEach(e => {
		acc[e] = (acc[e] || 0) + 1;
	});
	return acc;
}, {});

console.log(result);